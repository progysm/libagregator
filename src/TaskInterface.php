<?php

namespace ProgYSM\Agregator;

interface TaskInterface
{
    public function run(): int;
}