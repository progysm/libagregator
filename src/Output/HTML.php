<?php

namespace ProgYSM\Agregator\Output;

use DOMDocument;

class HTML extends AbstractOutput
{
    public function render(): string
    {
        $iFil = 0;
        $moisan = '';
    
        $header = '<div class="hfeed">'."\n";
        $footer = '</div>';

        $lines = [];
        foreach ($this->tabFil as $fil) {
            // special month break and start for HTML
            if ($moisan != substr($fil['DATE'], 0, 7)) {
                if ($moisan != '') {
                    $lines[] = '</ul>' . "\n";
                }
                $moisan = substr($fil['DATE'], 0, 7);
                $lines[] = '<p class="agmois">' . $this->html->html($moisan) . '</p>';
                $lines[] = '<ul>';
            }
            $lines[] = $this->renderEntry($fil);
    
            if ((++$iFil) >= $this->maxNewsDisplayed) {
                break;
            }
        }
        if ($lines) {
            $lines[] = '</ul>';
        }

        return $header . implode('', $lines) . $footer;
    }

    public function renderEntry(array $entry): string
    {
        $date = $this->html->html($entry['DATE']);
        $updatedDate = $this->html->html(date('c', strtotime($entry['DATE'])));
        $line = '<li class="hentry">';
        $line .= '<div class="title entry-title"><p><a href="' . $this->html->url($entry['LINK']) . '">' . $this->html->html($entry['TITLE']) . '</a></p></div>' . "\n";
        $line .= '<div class="divdate"><p><span class="date"><abbr class="updated" title="' . $updatedDate . '">' . $this->formatDate($date) . '</abbr></span>';
        $line .= '<span class="authorof"> ' . $this->html->__html('of') . ' </span><span class="author vcard"><span class="fn">' . $this->html->html($entry['CREATOR']) . '</span></span></p></div>' . "\n";

        $hasDesc = false;
        if (array_key_exists('CONTENT:ENCODED', $entry)) {
            $xmlString = $this->stripTagsDescription($entry['CONTENT:ENCODED']);
            if ($xmlString != '') {
                $line .= $xmlString;
                $hasDesc = true;
            }
        }
        if (!$hasDesc && $entry['DESCRIPTION'] != '') {
            $line .= '<div class="desc entry-content">' . nl2br($this->html->html(strip_tags($entry['DESCRIPTION']))) . "\n</div>\n";
        }
        if (count($entry['CATEGORY']) > 0) {
            $idx = 0;
            natcasesort($entry['CATEGORY']);
            $line .= ' <div class="categories">';
            foreach ($entry['CATEGORY'] as $category) {
                if ($idx != 0) {
                    $line .= ', ';
                }
                $idx++;
                $line .= '<a rel="tag">' . $this->html->html($category) . '</a>';
            }
            $line .= ' </div>' . "\n";
        }

        $line .= '</li>' . "\n";
        return $line;
    }

    /*
    * Retourne la date sous le bon format pour l'affichage
    * Pas de seconde, pas d'annee
    * @param $date  Format: 2005-01-01 12:00:00 -> 01 a 12h00
    */
    public function formatDate(string $date): string
    {
        $l = strlen($date);
        // enleve les secondes
        if ( 19 == $l ) {
            $date = substr($date, 0, $l-3);
        }
        // enleve le mois et l'annee
        // ATTENTION: presentement le serveur est sur l'heure de greenwitch
        $date = substr($date, 8, 2) . 
            $this->html->__html(' at ') . 
            substr($date, 11, 2) . 'h' . substr($date, 14, 2);
    
        return $date;
    }

    /**
     * Strips html tags 
     * @param string $htmlTags
     * @return string xml encoded with <div class="desc"></div> or empty string
     * @since 20090222
     */
    function stripTagsDescription(string $htmlTags): string
    {
        $htmlString = str_replace(' />', '/>', $htmlTags);
        $htmlString = strip_tags($htmlString, '<p><ul><li><ol><em><strong><blockquote><br/><br><code><pre>');
        $htmlString = preg_replace('#<([a-z]+)([^>]*)>#', '<${1}>', $htmlString);
        // pass it to xml
        $dom = new DOMDocument('1.0', 'UTF-8');
        if (@$dom->loadHTML('<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head><body><div class="desc">' . $htmlString . '</div></body></html>')) {
            $saved = $dom->saveXML($dom->documentElement->childNodes->item(1)->firstChild);
            if (is_string($saved)) {
                return $saved;
            }
        }
        return '';
    }
  
}