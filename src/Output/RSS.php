<?php

namespace ProgYSM\Agregator\Output;

class RSS extends AbstractOutput
{
    private $title = '';
    private $description = '';
    private $link = '';

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function setLink(string $link)
    {
        $this->link = $link;
    }

    public function render(): string
    {
        $header = '<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/">
        <channel>
        <title>' . $this->html->html($this->title) . '</title>
        <description>' . $this->html->html($this->description) . '</description>
        <link>' . $this->html->html($this->link) . '</link>
        ';
        $footer = '</channel></rss>';
    
        $iFil = 0;

        $lines = [];
    
        // each ...
        foreach ($this->tabFil as $fil) {
            $lines[] = $this->renderEntry($fil);
            if ( (++$iFil) >= $this->maxNewsDisplayed) {
                break;
            }
        }

        return $header . implode('', $lines) . $footer;
    }

    public function renderEntry(array $entry): string
    {
        $date = $this->formatDcDate($entry['DATE']);
        $line = '<item>'."\n";
        $line .= '  <title>'.$this->html->html($entry['TITLE']).'</title>'."\n";
        $line .= '  <link>'.$this->html->html($entry['LINK']).'</link>'."\n";
        $line .= '  <dc:creator>'.$this->html->html($entry['CREATOR']).'</dc:creator>'."\n";
        $line .= '  <dc:date>'.$this->html->html($date).'</dc:date>'."\n";
        if ($entry['DESCRIPTION'] != '') {
            $line .= '  <description>'.nl2br($this->html->html(strip_tags($entry['DESCRIPTION']))).'</description>'."\n";
        }
        $line .= '</item>'."\n";
        return $line;
    }

    private function formatDcDate(string $date)
    {
        return str_replace(' ', 'T', $date);
    }
}