<?php

namespace ProgYSM\Agregator\Output;

use ProgYSM\Agregator\UI\HTMLOutputInterface;

abstract class AbstractOutput
{
    private $tabFil = [];

    private $maxNewDisplayed = 0;

    /**
     * @var HTMLOutputInterface
     */
    private $html = null;

    public function __construct(array $tabFil, int $maxNewDisplayed, HTMLOutputInterface $html)
    {
        $this->tabFil = $tabFil;
        $this->maxNewDisplayed = $maxNewDisplayed;
        $this->html = $html;
    }

    public function write(string $filename): bool
    {
        if ($filename === '') {
            return false;
        }
        $content = $this->render();
        $written = file_put_contents($filename, $content);
        return $written !== false;
    }

    abstract public function render(): string;

    abstract public function renderEntry(array $entry): string;
}