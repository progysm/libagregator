<?php

namespace ProgYSM\Agregator\Output;

class HTMLTable extends AbstractOutput
{
    public function render(): string
    {
        $header = '<table summary="' . $this->html->__attr('RSS feeds list') . '">
        <thead><tr><th class="title">' . $this->html->__html('Title').'</th>
        <th class="description">' . $this->html->__html('Description') . '</th>
        </tr></thead>
        <tbody>';
        $footer = '</tbody></table>';

        $bodyAccumulator = [];
        $iFil = 0;
        foreach ($this->tabFil as $fil) {
            if ($iFil >= $this->maxNewsDisplayed) {
                break;                
            }
            $bodyAccumlator[] = $this->renderEntry($fil);
            $iFil++;
        }

        $body = implode('', $bodyAccumulator);

        return $header . $body . $footer;
    } 
 
    public function renderEntry(array $entry): string
    {
        $date = $this->formatDcDate($entry['DATE']);
        $line = '<tr>'."\n";
        $line .= '  <td class="info"><p class="title"><a href="' . $this->html->html($entry['LINK']).'">'. $this->html->html($entry['TITLE']).'</a></p>'."\n";
        $line .= '  <p class="dccreator">' . $this->html->html($entry['CREATOR']).'</p>'."\n";
        $line .= '  <p class="dcdate">' . $this->html->html($date).'</p></td>'."\n";
        if ($entry['DESCRIPTION'] != '') {
            $line .= '  <td class="description">'.nl2br($this->html->html(strip_tags($entry['DESCRIPTION']))).'</td>'."\n";
        } else {
            $line .= '  <td class="description"></td>'."\n";
        }
        $line .= '</tr>'."\n";
        return $line;
    }          

    private function formatDcDate(string $date)
    {
        return str_replace(' ', 'T', $date);
    }
}