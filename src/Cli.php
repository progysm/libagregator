<?php

namespace ProgYSM\Agregator;

class Cli
{
    /**
     * @param array [filename -c config.php confName]
     */
    public function run(array $argv = null): int
    {
        if ($argv === null) {
            $argv = $_SERVER['argv'] ?? [];
        }

        $confName = 'site';
        $options = [
            'conf.filename' => __DIR__ . '/config.php',
        ];

        // remove filename
        array_unshift($argv);

        if (isset($argv[0], $argv[1]) && $argv[0] === '-c') {
            array_unshift($argv);
            $options['conf.filename'] = array_unshift($argv);
            array_unshift($argv);
        }
        
        $confName = $argv[0] ?? '';

        if (!is_string($options['conf.filename']) || !file_exists($options['conf.filename'])) {
            echo "[error] Missing settings file (-c settings.php)\n";
            return -1;
        }

        $conf = include $options['conf.filename'];
        if (!is_array($conf)) {
            echo "[error] Invalid settings file (-c settings.php)\n";
            return -1;
        }

        $settings = new AgregatorSettings($conf);
        $configFile = new ConfigFile($confName, $settings->getString('config.path'));
        if (!$configFile->exists()) {
            echo "[error] Missing agregator configuration file\n";
            return -1;
        }

        $tr = new UI\Translator('libagregator', $settings->getString('language', 'en'));
        $html = new UI\HTMLOutput($tr);

        $task = new AgregatorTask($configFile, $html);
        return $task->run();
    }
}
