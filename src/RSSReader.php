<?php
/*
    YSM libagregator - a PHP agregator using cron
    Copyright (C) 2006,2007,2008,2022 Yan Morin <progysm@gmail.com>

    This file is part of YSM libagregator.

    YSM libagregator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    YSM libagregator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with YSM libagregator.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace ProgYSM\Agregator;

class RSSReader
{
    public const ERROR_FETCH_OPEN = 'fetch:fopen';
    public const ERROR_GETRSS_EMPTY = 'getRSS:empty';
    public const ERROR_GETRSS_PARSE = 'getRSS:parse';
    public const DEFAULT_ENCODING = 'UTF-8';

    private $error = '';
    private $log = [];

    private $userAgent = '';

    /*
    * Obtient un fichier a partir d'une URI
    * @param  $rssURI Adresse du fichier 
    * @param  $outputEncoding Encodage de sortie (defaut:utf-8)
    * @return string
    */
    public function fetch(string $rssURI, string $outputEncoding = self::DEFAULT_ENCODING): string
    {
        $returnValue = ''; // chaine du fichier
        $inputEncoding = $outputEncoding; // chaine du fichier d'entree

        $context_http_header = '';
        if ($this->userAgent !== '') {
            $context_http_header .= 'User-agent: ' . $this->userAgent . "\r\n";
        }
        // note: http are good for http:// and https:// scheme
        $opts = array('http' => array('method' => 'GET', 'header' => $context_http_header));
        $context = stream_context_create($opts);
        $fp = @fopen($rssURI, 'r', false, $context);
        if (!$fp) {
            $this->error = self::ERROR_FETCH_OPEN;
            return $returnValue;
        }
        while (!feof($fp)) {
            $returnValue .= fread($fp, 1024);
        }
        fclose($fp);

        $start = substr($returnValue, 0, 100);
        // 100 est une valeur arbitraire, j'espere avoir le <xml> dedans.

        if (preg_match('/<\?xml.*encoding=["\']([^"\']+)["\'].*\?>/', $start, $tok)) {
            $tok[1] = strtoupper($tok[1]);
            if ($tok[1] === self::DEFAULT_ENCODING) { // seulement un encodage...
                $inputEncoding = $tok[1];
            }
        }

        if (strtolower($inputEncoding) != strtolower($outputEncoding)) {
            #echo "DEBUG: converting $URI from $inputEncoding to $outputEncoding";
            $returnValue = utf8_encode($returnValue);
        }
        return $returnValue;
    }

    /**
     * Affiche un fil RSS avec le titre et les item (LINK/TITLE/DESCRIPTION)
     * @param string $rssURI Fil RSS à ouvrir
     * @param string $title  (out) Retourne le titre du fil
     * @param array $options doit contenir (si vide, les défauts sont utilisé)
     *                 entities => indique si on doit convertir les entités si true [defaut:false]
     *                 description => champ description à conserver  [default:true]
     *                 max    => nombre maximum de message à retirer [default:4]
     *         modifié le 2006-01-31, problème SPAM dans une desc sur loli....
     * @return array list of feed with array('LINK'=>'', 'TITLE'=>'', 'DATE'=>'');
     * date, dc:date ou pubdate
     * date préférée: YYYY-MM-DD HH:SS:MM
     */
    public function getRSS(string $rssURI, string $rssTitle, array $options): array
    {
        $this->error = '';
        $this->log = [];

        $returnValue = [];
        $opt = is_array($options) && count($options) ? $options : array('entities'=>false,'description'=>true,'max'=>4);

        //  $outputEncoding = 'iso-8859-1';
        $outputEncoding = 'UTF-8'; // doit etre en utf-8 pour PHP5
        $simple = $this->fetch($rssURI, $outputEncoding);
        if ($this->error !== '') {
            return $returnValue;
        }

        if ($simple == '') {
            $this->error = self::ERROR_GETRSS_EMPTY;
            return $returnValue;
        }

        $fieldList = ['LINK', 'TITLE', 'DATE', 'DC:DATE', 'PUBDATE', 'PUBLISHED', 'DC:CREATOR', 'CATEGORY'];
        if ($opt['description'] === true) {
            $fieldList[] = 'DESCRIPTION';
            $fieldList[] = 'CONTENT:ENCODED';
        }

        $p = xml_parser_create($outputEncoding);
        $returnValueXML = @xml_parse_into_struct($p, $simple, $vals, $index);
        if (0 === $returnValueXML) {
            $this->log[] = "RSSReader::getRSS(" . $rssURI . ")" . 'line ' . xml_get_current_line_number($p);
            $this->log[] = 'error: ' . xml_error_string(xml_get_error_code($p));
            $errorString = 'getRSS:parse';
            xml_parser_free($p);
            return $returnValue;
        }
        xml_parser_free($p);


        // le code pour afficher titre du rss pourrait ressembler à ceci:
        if (isset($index['TITLE']) && is_array($index['TITLE']) && count($index['TITLE'])) {
            $titleTag = $vals[$index['TITLE'][0]];
            if ($titleTag['level'] == 3) {
                $rssTitle = $titleTag['value'];
            }
        }

        $tblItem = false;
        if (isset($index['ITEM'])) {
            $tblItem = $index['ITEM'];
        }
        if (!is_array($tblItem)) {
            return $returnValue; // écrit rien, s'il n'y a pas d'items
        }

        $i = 1;
        foreach ($tblItem as $no) {
            if ($vals[$no]['type'] == 'open') {
                // condition pour un nombre limite...
                if ($i <= $opt['max']) {
                    $i++;
                } else {
                    break;
                }

                $news = array();
                $news['CATEGORY'] = array();
                while (isset($vals[++$no]) && $vals[$no]['type'] != 'close') {
                    if (in_array($vals[$no]['tag'], $fieldList) && isset($vals[$no]['value'])) {
                        if ($vals[$no]['tag'] == 'CATEGORY') {
                            $news['CATEGORY'][] = $vals[$no]['value'];
                        } else {
                            $news[$vals[$no]['tag']] = $vals[$no]['value'];
                        }
                    }
                }

                // 2006-05-13 : bloque del.ici.us links
                if (isset($news['DC:CREATOR'])) {
                    if (
                        strstr($news['DC:CREATOR'], 'delicious') !== false
                        || strstr($news['DC:CREATOR'], 'del.icio.us') !== false
                    ) {
                        continue;
                    }
                }

                // si on n'a pas deja une date
                if (!isset($news['DATE']) || $news['DATE'] == '') {
                    // on regarde si on aurait pas une dc:date
                    if (isset($news['DC:DATE']) && $news['DC:DATE'] != '') {
                        $news['DATE'] = $news['DC:DATE'];
                    } elseif (isset($news['PUBDATE'])) {
                        $timestamp = strtotime($news['PUBDATE']);
                        if (-1 != $timestamp) {
                            $news['DATE'] = date('Y-m-d H:i:s', $timestamp);
                        }
                    } elseif (isset($news['PUBLISHED'])) {
                        $timestamp = strtotime($news['PUBLISHED']);
                        if (-1 != $timestamp) {
                            $news['DATE'] = date('Y-m-d H:i:s', $timestamp);
                        }
                    } elseif (array_key_exists('LINK', $news) && preg_match('#/(\d{4})/(\d{2})/(\d{2})#', $news['LINK'], $tokenLink)) {
                        // sometime date is in url
                        $news['DATE'] = $tokenLink[1] . '-' . $tokenLink[2] . '-' . $tokenLink[3] . ' 00:00:00';
                    }
                }
                if (!array_key_exists('DESCRIPTION', $news)) {
                    $news['DESCRIPTION'] = '';
                }
                $md5 = $news['LINK'];

                if ($opt['entities']) {
                    $news['TITLE'] = html_entity_decode($news['TITLE'], ENT_NOQUOTES, $outputEncoding);
                    $news['DESCRIPTION'] = html_entity_decode($news['DESCRIPTION'], ENT_NOQUOTES, $outputEncoding);
                }
                $returnValue[$md5] = $news;
            } // fin open/close - item

        } // fin chaque item
        return $returnValue;
    }

    public function setUserAgent(string $userAgent)
    {
        $this->userAgent = $userAgent;
    }

    public function getError(): string
    {
        return $this->error;
    }

    public function getLog(): array
    {
        return $this->log;
    }
}