<?php

namespace ProgYSM\Agregator;

use ProgYSM\Agregator\UI\HTMLOutputInterface;

class AgregatorTask implements TaskInterface
{
    /**
     * @var ConfigFile
     */
    private $configFile = null;

    /**
     * @var HTMLOutputInterface
     */
    private $html = null;

    public function __construct(ConfigFile $configFile, HTMLOutputInterface $html)
    {
        $this->configFile = $configFile;
        $this->html = $html;
    }

    public function log(string $s)
    {
        error_log($s);
    }

    /**
     * Fonctionnement général
     * Pour chaque site dans $tabFilInfo
     *  1.1 obtenir
     *  1.2 convertir en utf-8
     *  1.3 envoyer le LINK/TITLE/DATE
     *
     *  2.1 parcourir les résultas obtenus
     *  2.2 modifier la date, en format actuel sans T, sans +00:00
     *  2.3 supprimer les fil sans DATE
     *
     *  3.1 Concatener les nouvelles de chaque site
     * Trier par ordre du plus recent au plus ancien
     * Pour les MANEWSIL_DISPLAYED premiers fils dans les resultat ($tabFil)
     *   Ecrire dans le fichier de sortie OUTPUT_FILE
     */
    public function run(): int
    {
        if (!$this->configFile->load()) {
            return -1;
        }

        $dateToday = date('Y-m-d');
        $tabFil = $this->fetchAllRSS($this->configFile->getTabFilInfo(), $dateToday);

        $shouldRewriteConf = $this->configFile->getDefine('AGREGATOR_SHOULD_REWRITECONF');
        $maxNewsDisplayed = (int) $this->configFile->getDefine('MAX_NEWS_DISPLAYED');
        $outputHtmlFormat = $this->configFile->getDefine('OUTPUT_HTML_FORMAT');
        $outputFile = $this->configFile->getDefine('OUTPUT_FILE');
        $outputRssFile = $this->configFile->getDefine('OUTPUT_RSS_FILE');
        if ($shouldRewriteConf === '1') {
            $this->rewriteConf($tabFil, $maxNewsDisplayed);
        }

        if ($outputHtmlFormat === ConfigFile::OUTPUT_HTML_FORMAT_TABLE) {
            $htmlTable = new Output\HTMLTable($tabFil, $maxNewsDisplayed, $this->html);
            $htmlTable->write($outputFile);
        } else {
            $htmlOutput = new Output\HTML($tabFil, $maxNewsDisplayed, $this->html);
            $htmlOutput->write($outputFile);
        }

        $rss = new Output\RSS($tabFil, $maxNewsDisplayed, $this->html);
        $rss->setTitle($this->configFile->getDefine('HTML_TITLE'));
        $rss->setDescription($this->configFile->getDefine('HTML_DESCRIPTION'));
        $rss->setLink($this->configFile->getDefine('HTML_LINK'));
        $rss->write($outputRssFile);
        return 0;
    }

    private function fetchAllRSS(array $tabFilInfo, string $dateToday): array
    {
        $tabFil = [];
        foreach ($tabFilInfo as $indexFilInfo => $filInfo) {
            $rssURI = $filInfo[0];
            if ($rssURI=='' || (isset($filInfo['disabled']) && $filInfo['disabled']==true)) {
                continue;
            }
            $entities = isset($filInfo['entities']) && $filInfo['entities'] ? true : false;
            $desc     = (isset($filInfo['description']) && $filInfo['description'] === false) ? false : true; 
          
            $options = array('entities'=>$entities, 'description'=>$desc,'max'=>5);
             
            //echo $filInfo[0] . "\n";
            $rssTitle = '';
            $rssReader = new RSSReader();
            $rssReader->setUserAgent($this->settings->getString('useragent'));
            $tab = $rssReader->getRSS($rssURI, $rssTitle, $options);
            $errorString = $rssReader->getError();

            // modify current tab
            // echo $filInfo[0]."\n";
            $tabFilInfo[$indexFilInfo]['count'] = count($tab);
            $tabFilInfo[$indexFilInfo]['lastcount'] = 0;
            $tabFilInfo[$indexFilInfo]['lastdate'] = '';
            $tabFilInfo[$indexFilInfo]['error'] = $errorString;
            if ($errorString == '') {
                $tabFilInfo[$indexFilInfo]['errorTimes'] = 0;
            } else {
                $tabFilInfo[$indexFilInfo]['errorTimes'] = isset($filInfo['errorTimes']) ? intval($filInfo['errorTimes'])+1 : 1;
            }
          
            if ($errorString !== '') {
                switch($errorString) {
                    case RSSReader::ERROR_FETCH_OPEN:
                        $this->log('agregator error: open ' . $rssURI);
                        break;
                    case RSSReader::ERROR_GETRSS_EMPTY:
                        $this->log('agregator error: '. $rssURI . ' is empty');
                        break;
                    case RSSReader::ERROR_GETRSS_PARSE:
                        $this->log('agregator error: '. $rssURI . ' xml is invalid');
                        break;
                }
            } else {
                // insertion du nom du site
                foreach($tab as $key => $fil) {
                    $tab[$key]['SITE'] = $filInfo[1];
          
                    if ( isset($tab[$key]['DC:CREATOR']) && $tab[$key]['DC:CREATOR'] != '' ) {
                        $tab[$key]['CREATOR'] = $tab[$key]['DC:CREATOR'];
                    } else {
                        $tab[$key]['CREATOR'] = $tab[$key]['SITE'];
                    }
            
                    // correction de la date
                    if ( isset($fil['DATE']) ) {
                        // si 25 avec +00:00, on enleve le T et le +00:00
                        if ( strlen($fil['DATE']) == 25 && substr($fil['DATE'],-5) == '00:00' ) {
                            $tab[$key]['DATE'] = substr($fil['DATE'],0,19);
                        }
                        // on enleve les dates > qu'aujourd'hui
                        if (substr($fil['DATE'],0,10) > $dateToday) {
                            unset($tab[$key]);
                        } else {
                            $tab[$key]['DATE'] = str_replace('T', ' ',$tab[$key]['DATE']);
                        }
                    } else {
                        unset($tab[$key]);
                    }
                }
          
                if (count($tab)) {
                    // adding index related
                    foreach($tab as $idx => $fil) {
                        $tab[$idx]['parentindex'] = $indexFilInfo;
                    }
                    $tabFil = array_merge($tabFil, $tab);
                }
            }
        } 
          
        // Sort by comparing feeds by date order (newer on top)
        usort($tabFil, function ($a, $b) {
            return strcmp($b['DATE'], $a['DATE']);
        });
        return $tabFil;
    }

    private function rewriteConf(array $tabFil, int $maxNewsDisplayed)
    {
        $i = 0;
        foreach($tabFil as $fil) {
          if ($i < $maxNewsDisplayed) {
            $this->configFile->incrementLastCount($fil['parentindex']);
          }
          $this->configFile->updateLastDate($fil['parentindex'], $fil['DATE']);
          $i++;
        }
        $this->configFile->save();
    }
}