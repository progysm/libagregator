<?php

namespace ProgYSM\Agregator;

class AgregatorSettings implements SettingsInterface
{
    private $configs = [
        'config.path' => '',
        'language' => 'en',
        'useragent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0',
    ];

    public function __construct(array $config = [])
    {
        $this->loadConfig($config);
    }

    public function loadConfig(array $config = [])
    {
        foreach ($config as $name => $value) {
            if (key_exists($name, $this->configs)) {
                $this->config[$name] = $value;
            }
        }
    }

    public function getString(string $name, string $defaultValue = ''): string
    {
        if (key_exists($name, $this->configs) && is_string($this->config[$name])) {
            return $this->config[$name];
        }
        return $defaultValue;
    }
}