<?php

namespace ProgYSM\Agregator;

interface SettingsInterface
{
    public function getString(string $name, string $defaultValue = ''): string;
}