<?php

namespace ProgYSM\Agregator;

class ConfigFile
{
    public const OUTPUT_HTML_FORMAT_TABLE = 'TABLE';
    public const OUTPUT_HTML_FORMAT = 'HTML';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $path = '';

    /**
     * @var array
     */
    private $defines = [];

    /**
     * @var array
     */
    private $tabFilInfo = [];

    public function __construct(string $name = '', string $path = '')
    {
        $this->name = $name;
        $this->path = $path;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function exists(): bool
    {
        $filename = $this->getFilename();
        if ($filename === '') {
            return false;
        }

        return file_exists($filename);
    }

    public function canWrite(): bool
    {
        $filename = $this->getFilename();
        if ($filename === '') {
            return false;
        }
        return is_writable($filename);
    }

    public function getFilename(string $ext = 'json'): string
    {
        if ($this->name === '' || $this->path === '' || $ext === '') {
            return '';
        }
        return $this->path . '/' . $this->name . '.' . $ext;
    }

    public function getDefaultDefines(): array
    {
        return 	[
            'MAX_NEWS_DISPLAYED'=>'30',
            'OUTPUT_FILE'=>'', 
            'OUTPUT_RSS_FILE'=>'', 
            'OUTPUT_HTML_FORMAT'=>self::OUTPUT_HTML_FORMAT,
            'HTML_TITLE'=>'titre',
            'HTML_DESCRIPTION'=>'description',
            'HTML_LINK'=>'link',
            'AGREGATOR_SHOULD_REWRITECONF'=>'0'
        ];
    }

    public function getDefines(): array
    {
        return $this->defines;
    }

    public function getDefine(string $name): string
    {
        return $this->defines[$name] ?? '';
    }

    public function setDefine(string $name, $value)
    {
        if (!is_string($value)) {
            return;           
        }
        if (!key_exists($name, $this->getDefaultDefines())) {
            return;
        }
        $this->defines[$name] = $value;
    }

    public function getTabFilInfo(): array
    {
        return $this->tabFilInfo;
    }

    public function setTabFilInfo(array $tabFilInfo)
    {
        $this->tabFilInfo = $tabFilInfo;
    }

    /**
     * @return array
     */
    public function getInfo(): array
    {
        return [
            'name' => $this->name,
            'exists' => $this->exists(),
            'defines' => array_merge($this->getDefaultDefines(), $this->getDefines()),
            'tabFilInfo' => $this->getTabFilInfo(),
        ];
    }

    public function load(): bool
    {
        $filename = $this->getFilename();
        if ($filename === '') {
            return false;
        }

        if (!is_readable($filename)) {
            return false;
        }

        $content = file_get_contents($filename);
        if ($content === false || $content === '') {
            return false;
        }

        try {
            $json = json_decode($content, true,  JSON_THROW_ON_ERROR);
            if (is_array($json)) {
                $this->loadArray($json);
                return true;
            }
        } catch (\Exception $e) {
        }
        return false;
    }

    public function loadPHP()
    {
        $filename = $this->getFilename('php');        
        if (is_readable($filename)) {
            require_once $filename;

            $this->defines = [];
            foreach ($this->getDefaultDefines as $constantName => $value) {
                $value = defined($constantName) ? constant($constantName) : $value;
                $this->defines = (string) $value;
            }

            $this->tabFilInfo = isset($tabFilInfo) && is_array($tabFilInfo) ? $tabFilInfo : [];
        }
    }

    private function loadArray(array $arr)
    {
        $this->defines = [];
        if (isset($arr['defines']) && is_array($arr['defines'])) {
            $defines = $this->getDefaultDefines();
            foreach ($defines as $name => $value) {
                if (isset($arr['defines'][$name]) && is_string($arr['defines'][$name])) {
                    $defines[$name] = $arr['defines'][$name];
                }
            }

            $this->defines = $defines;
        }

        $this->tabFilInfo = isset($arr['tabFilInfo']) && is_array($arr['tabFilInfo']) ? $arr['tabFilInfo'] : [];
    }

    public function getContents(): string
    {
        $filename = $this->getDefine('OUTPUT_FILE');
        if ($filename === '' || !file_exists($filename)) {
            return '';
        }
        $contents = file_get_contents($filename);
        if ($contents === false) {
            return '';
        }
        return $contents;
    }

    public function save(): bool
    {
        $filename = $this->getFilename();
        if ($filename === '') {
            return false;
        }

        if (!is_writable($filename)) {
            return false;
        }

        $contents = [
            'name' => $this->name,
            'defines' => array_merge($this->getDefaultDefines(), $this->getDefines()),
            'tabFilInfo' => $this->tabFilInfo,
        ];

        $data = file_put_contents($filename, json_encode($contents, JSON_PRETTY_PRINT));
        return $data !== false && $data > 0;
    }

    public function saveCopy(): bool
    {
        $filename = $this->getFilename();
        if ($filename === '') {
            return false;
        }
        $to = $this->getFilename('.bak.json');
        return copy($filename, $to);
    }

    public function incrementLastCount(int $index)
    {
        if (!isset($this->tabFilInfo[$index])) {
            return;
        }
        if (!isset($this->tabFilInfo[$index]['lastcount'])) {
            $this->tabFilInfo[$index]['lastcount'] = 0;
        }
        $this->tabFilInfo[$index]['lastcount']++;
    }

    public function updateLastDate(int $index, string $newDate)
    {
        if (!isset($this->tabFilInfo[$index])) {
            return;
        }

        $this->tabFilInfo[$index]['lastdate'] = $newDate;
    }
}