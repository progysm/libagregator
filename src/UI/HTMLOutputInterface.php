<?php

namespace ProgYSM\Agregator\UI;

interface HTMLOutputInterface
{
    /**
     * Escape HTML
     */
    public function html(string $html);

    /**
     * Escape attribute value
     */
    public function attr(string $value);

    /**
     * Escape URL in attribute
     */
    public function url(string $url);

    /**
     * Translate and escape html
     */
    public function __html(string $html);

    /**
     * Translate and escape attribute
     */
    public function __attr(string $value);
}