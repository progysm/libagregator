<?php

namespace ProgYSM\Agregator\UI;

class HTMLOutput implements HTMLOutputInterface
{
    /**
     * @var TranslatorInterface|null $translator
     */
    protected $translator = null;

    public function __construct(TranslatorInterface $translator = null)
    {
        $this->translator = $translator;
    }

    /**
     * Escape HTML
     */
    public function html(string $html)
    {
        return htmlspecialchars($html);
    }

    /**
     * Escape attribute value
     */
    public function attr(string $value)
    {
        return $this->html($value);
    }

    /**
     * Escape URL in attribute
     */
    public function url(string $url)
    {
        return $this->html($url);
    }

    public function __html(string $html)
    {
        return $this->html($this->__($html));
    }

    public function __attr(string $value)
    {
        return $this->attr($this->__($value));
    }

    public function __($s)
    {
        if (!$this->translator) {
            return $s;
        }
        return $this->translator->__($s);
    }
}