<?php

namespace ProgYSM\Agregator\UI;

use ProgYSM\Agregator\ConfigFile;
use ProgYSM\Agregator\ConfigFileList;
use ProgYSM\Agregator\SettingsInterface;

class AdminController extends Controller
{
    private const PARAM_ACTION_NAME = 'agregatorAction';

    /**
     * @param SettingsInterface
     */
    private $settings = null;

    public function setSettings(SettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    public function getSetting(string $name, string $defaultValue = '')
    {
        return $this->settings ? $this->settings->getString($name, $defaultValue) : $defaultValue;
    }

    public function runRequest()
    {
        $action = 'default';
        $agregatorAction = (string) ($_GET[self::PARAM_ACTION_NAME] ?? '');
        $configFile = (string) ($_POST['configFile'] ?? $_GET['configFile'] ?? '');

        return $this->run('default', [$configFile, $agregatorAction, $_POST]);
    }

    /**
     * @param string $configFile
     * @param string $action ''|run|viewhtml
     */
    public function defaultAction(string $configFile = '', string $action = '', array $post = [])
    {
        $configPath = $this->getSetting('config.path');
        $tr = new Translator('libagregator', $this->getSetting('language', 'en'));

        $html_link = '';
        $errorList = [];

        // TODO handle update if configFile and $post
        if ($configFile !== '' && isset($post['DEFINETXT'])) {
            $configFile = new ConfigFile($configFile, $configPath);
            $errorList = $this->saveConfig($configFile, $post, $tr);
        }

        if ($action === 'run') {
            // TODO
            $_SERVER['argv'] = array(0=>'agregator.php', 1=>$configFile);
            require_once dirname(__FILE__) . '/agregator.php';
        }

        $configFile = new ConfigFile($configFile, $configPath);
        $configFile->load();
        $config = $configFile->getInfo();

        $html = new HTMLOutput($tr);
        $configFileList = [];
        $formActionAddLink = $html_link;

        // config loaded part
        $runLink = '';
        $viewLink = '';
        $outputHTMLContent = null;

        // admin.config.php
        $formActionSaveLink = $html_link;

        if ($config && $config['exists']) {
            if ($action === 'run' || $action === 'viewhtml') {
                // TODO REPLACE THAT
                $outputHTMLContent = $configFile->getContents();
            }

            $runLink = $html_link . '?' . http_build_query(['configFile' => $config['name'], self::PARAM_ACTION_NAME => 'run']);
            $viewLink = $html_link .'?' . http_build_query(['configFile' => $config['name'], self::PARAM_ACTION_NAME => 'viewhtml']);
            $formActionSaveLink = $html_link;

            $tabFilInfo = $config['tabFilInfo'];
            usort($tabFilInfo, function ($a, $b) {
                return strcasecmp($a[1], $b[1]);
            });
            $config['tabFilInfo'] = $tabFilInfo;
        }

        try {
            $configFileListObject = new ConfigFileList($configPath);
            $configFileNames = $configFileListObject->getFileNames();
            foreach ($configFileNames as $fileName) {
                $fileBasename = basename($fileName, '.json');
                $selected = $fileBasename === $config['name'];
                $configFileList[] = [
                    'filename' => $fileName,
                    'title' => $fileBasename,
                    'selected' => $selected,
                    'className' => $selected ? 'current' : '',
                    'url' => $html_link . '?' . http_build_query(['configFile' => $fileBasename]),
                ];
            }
        } catch (\InvalidArgumentException $e) {
            $errorList[] = $tr->translate('Invalid configuration of variable $configPath : "$0"', $configPath);
        }

        // render here
        include __DIR__ . '/../../templates/admin.ui.php';
        return null;
    }

    private function saveConfig(ConfigFile $configFile, array $post, TranslatorInterface $tr): array
    {
        $errorList = [];
        if (!$configFile->canWrite()) {
            $errorList[] = $tr->translate('Configuration file "$0" is not readable and writable.', $configFile->getName());
            return $errorList;
        }

        if (
            isset($post['DEFINETXT'], $post['DEFINE']) &&
            is_array($post['DEFINETXT']) && 
            is_array($post['DEFINE'])
        ) {
            foreach ($post['DEFINETXT'] as $i => $defineName) {
                $configFile->setDefine($defineName, $post['DEFINE'][$i] ?? null);
            }
        }

        $tmpTabFilInfo = [];
        for($iFil = 0; $iFil < $post['newnbrss']; $iFil++) {
          if ($post['newrss'.$iFil] !== '') {
            $tmpTabFilInfo[] = [
                $post['newrss'.$iFil], 
                $post['newname'.$iFil], 
                $post['newsite'.$iFil], 
                'entities'=>isset($post['newentities'.$iFil]), 
                'description'=>isset($post['newdescription'.$iFil]),
                'error'=>'',
                'errorTimes'=>0,
                'count'=>0,
                'lastcount'=>0,
                'lastdate'=>''
            ];
          }
        }

        for($iFil = 0; $iFil < $post['nbrss']; $iFil++) {
          if ($post['rss'.$iFil] !== '') {
            $tmpTabFilInfo[] = [
                $post['rss'.$iFil], 
                $post['name'.$iFil], 
                $post['site'.$iFil], 
                'entities'=>isset($post['entities'.$iFil]), 
                'description'=>isset($post['description'.$iFil]),
                'error'=>$post['error'.$iFil],
                'errorTimes'=>intval($post['errorTimes'.$iFil]),
                'count'=>intval($post['count'.$iFil]),
                'disabled'=>isset($post['disabled'.$iFil]),
                'lastcount'=>intval($post['lastcount'.$iFil]),
                'lastdate'=>$post['lastdate'.$iFil]
            ];
          }
        }

        $configFile->setTabFilInfo($tmpTabFilInfo);

        if (!$configFile->saveCopy()) {
            $errorList[] = $tr->__('Error saving copy config file');
        }
        if (!$configFile->save()) {
            $errorList[] = $tr->__('Error saving config file');
        }
        return $errorList;
    }
}
