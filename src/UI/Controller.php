<?php

namespace ProgYSM\Agregator\UI;

/**
 * Run actions method
 */
abstract class Controller
{
    abstract public function runRequest();

    /**
     * Run some internal methodAction
     * @param string $action
     */
    public function run(string $action, array $params)
    {
        if ($action === '') {
            $action = 'default';
        }
        $methodName = $action . 'Action';
        if (method_exists($this, $methodName)) {
            return $this->$methodName(...$params);
        }
        return null;
    }
}