<?php

namespace ProgYSM\Agregator\UI;

interface TranslatorInterface
{
    /**
     * Translate a string with $0, $1, $2, ...
     * @param string $s
     * @param mixed ...$values
     */
    public function translate(string $s, ...$values): string;

    /**
     * @param string $s
     */
    public function __(string $s): string;
}