<?php

namespace ProgYSM\Agregator\UI;

class Translator implements TranslatorInterface
{
    /**
     * @var string
     */
    private $domain = '';

    /**
     * @var string
     */
    private $language = '';

    /**
     * @var array|null
     */
    private $strings = null;

    public function __construct(string $domain = '', string $language = '')
    {
        $this->domain = $domain;
        $this->language = $language;
    }

    public function loadStrings()
    {
        $this->strings = [];
        $filename = __DIR__ . '/../../po/lang_' . basename($this->language) . '.php';
        if (!file_exists($filename)) {
            return;
        }
        $strings = include $filename;
        if (!is_array($strings)) {
            return;
        }
        $this->strings = $strings;
    }

    /**
     * @param string $s
     * @param mixed ...$values
     */
    public function translate(string $s, ...$values): string
    {
        $string = $this->__($s);
        foreach ($values as $index => $value) {
            $string = str_replace('$' . $index, $value, $string);
        }
        return $string;
    }

    /**
     * @param string $s
     */
    public function __(string $s): string
    {
        if (is_null($this->strings)) {
            $this->loadStrings();
        }
        return $this->strings[$s] ?? $s;
    }
}