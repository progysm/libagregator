<?php

namespace Progysm\Agregator;

use InvalidArgumentException;

class ConfigFileList
{
    private $path = '';

    /**
     * @param string $path
     */
    public function __construct(string $path)
    {
        return $this->path = $path;
    }

    /**
     * Get the list of filenames in the directory
     * @return array [filenames, ...]
     * @throws InvalidArgumentException
     */
    public function getFileNames(): array
    {
        if ('' === $this->path || !is_dir($this->path)) {
            throw new InvalidArgumentException();
        }

        $dh = opendir($this->path);
        if (!$dh) {
            return [];
        }

        $returnArray = [];
        while (($file = readdir($dh)) !== false) {
            if (is_writable($this->path . '/' . $file) && substr($file, -5) === '.json') {
                $returnArray[] = $file;
            }
        }
        closedir($dh);
        return $returnArray;
    }
}