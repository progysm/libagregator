<?php

/*
    YSM libagregator - a PHP agregator using cron
    Copyright (C) 2006,2007,2008,2022 Yan Morin <progysm@gmail.com>

    This file is part of YSM libagregator.

    YSM libagregator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    YSM libagregator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with YSM libagregator.  If not, see <http://www.gnu.org/licenses/>.
*/
return [
 'Configuration file "$0" is not readable and writable.'=>'Le fichier de configuration "$1" ne peut être lu ou écrit.'
,'Invalid configuration of variable $gConfigPath : "$0"'=>'La configuration de la variable $gConfigPath est invalide : "$1"'
,'Configuration file'=>'Fichiers de configurations'
,'New'=>'Nouveau'
,'Create'=>'Créer'
,'Configuration is loaded'=>'La configuration est chargée.'
,'Global Settings'=>'Configuration globale'
,'RSS feeds list'=>'Liste des fils RSS'
,'RSS Title'=>'Titre du RSS'
,'Entities'=>'Entité'
,'Description'=>'Description'
,'Disabled'=>'Inactif'
,'Error / times'=>'Erreur / nombre de fois'
,'Number of Items'=>'Nombre d\'items'
,'Last count / date'=>'Items affichés / date'
,'Save'=>'Enregistrer'
,'Errors'=>'Erreurs'
,'of'=>'de'
,'Title'=>'Titre'
,'Description'=>'Description'
,' at '=>' &agrave; '
];