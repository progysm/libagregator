# ProgYSM Agregator

This is a rewrite of libagregator. Original library was using a .php configuration file instead of json.

## Configuration
* config.path (necessary)
* language
* useragent

## Load the admin view/interface

### With default $_GET/$_POST)
```php
$settings = new \ProgYSM\Agregator\AgregatorSettings([
    'config.path' => '/directory/config',
    'language' => 'en',
    'useragent' => 'Some Agent 1.0',
]);

$controller = new \ProgYSM\Agregator\UI\AdminController();
$controller->setSettings($settings);
$controller->runRequest();
```

## CLI

Usage: php file.php -c config.php confName

* config.php should contains at least the instruction return ['config.path' => 'directory'];

```php
#!/usr/bin/php

if (php_sapi_name !== 'cli') {
    exit(-1);
}

$cli = new \ProgYSM\Agregator\Cli();
exit($cli->run());
```

## Inclusion with composer
```bash
composer config repositories.libagregator vcs https://gitlab.com/progysm/libagregator
composer require progysm/libagregator:dev-main
```
