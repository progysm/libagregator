<?php

namespace ProgYSM\Agregator\Tests;

use ReflectionClass;
use ReflectionMethod;

abstract class AbstractTest
{
    public function run()
    {
        $list = [];
        $reflectionClass = new ReflectionClass($this);
        $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach ($methods as $method) {
            if (substr($method->name, 0, 4) === 'test') {
                $list[] = $method->name;
            }
        }
        foreach ($list as $method) {
            $this->$method();
        }
    }

    protected function assertEquals($a, $b, $message)
    {
        if ($a !== $b) {
            echo $a, " and ", $b, " are not equals. ", $message, "\n";
        }
    }
}