<?php

namespace ProgYSM\Agregator\Tests;

use ProgYSM\Agregator\AgregatorSettings;
use ProgYSM\Agregator\UI\AdminController;

class UIAdminControllerTest extends AbstractTest
{
    public function testRunRequest()
    {
        $settings = new AgregatorSettings([
            'config.path' => __DIR__ . '/configs',
            'language' => 'fr',
            'useragent' => 'Some Agent 1.0',
        ]);
        
        $controller = new AdminController();
        $controller->setSettings($settings);
        $_GET['configFile'] = 'example';
        $controller->runRequest();
    }
}