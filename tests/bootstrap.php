<?php

/**
 * Bootstrap for tests, only run in cli
 * @usage php bootstrap.php SomeTest.php
 */

if (php_sapi_name() !== 'cli') {
    exit(-1);
}

require_once __DIR__ . '/../vendor/autoload.php';

$file = $_SERVER['argv'][1] ?? '';
$className = basename($file, '.php');
if (!file_exists('./' . $className . '.php')) {
    echo 'Missing filename', "\n";
    exit(-1);
}

$fullClassName = 'ProgYSM\\Agregator\\Tests\\' . $className;

if (!class_exists($fullClassName)) {
    echo 'Class not found', "\n";
    exit(-1);
}

$runner = new $fullClassName();
$runner->run();