<?php

/**
 * @param HTMLOutputInterface $html
 * @param array $configFileList list of filename, title, selected, className (current or ''), url
 * @param array $errorList list of errors (string)
 * @param string $formActionAddLink url for form
 * @param array $config empty array or with name, exists, defines, tabFilInfo
 * @param string $runLink
 * @param string $viewLink
 * @param string|null outputHTMLContent
 * @param string $formActionSaveLink url for form
 */
?>
<div id="text">
    <h2><?php echo $html->__html('Configuration file') ?></h2>
    <?php if ($errorList) { ?>
    <div id="error">
        <h3><?php echo $html->__html('Errors') ?></h3>
        <ul>
            <?php foreach ($errorList as $errorString) { ?>
            <li><?php echo $html->html($errorString) ?></li>
            <?php } ?>
        </ul>
    </div>
    <?php } else { ?>
    <div id="agregatornavlist">
        <h3><?php echo $html->__html('Configuration file') ?></h3>
        <ul>
            <?php foreach ($configFileList as $configFile) { ?>
            <li class="<?php echo $html->attr($configFile['className']) ?>"><a href="<?php echo $html->url($configFile['url']) ?>"><?php echo $html->html($configFile['title']) ?></a></li>
            <?php } ?>
        </ul>
        <form method="get" action="<?php echo $html->url($formActionAddLink) ?>"><p>
            <label for="configFileID"><?php echo $html->__html('New') ?></label>
            <input type="text" name="configFile" id="configFileID" value="" />
            <input type="submit" value="<?php echo $html->__attr('Create') ?>" />
        </p></form>
        <?php if ($config) { ?>
        <div id="agregatorconfig">
            <?php if ($config['exists']) { ?>
                <p><?php echo $html->__html('Configuration is loaded') ?></p>
                <p><a href="<?php echo $html->url($runLink) ?>"><?php echo $html->__html('Run') ?></a> | 
                <a href="<?php echo $html->url($viewLink) ?>"><?php echo $html->__html('View HTML') ?></a></p>
            <?php } ?>

            <?php if (isset($outputHTMLContent)) { ?>
                <div id="viewhtml" style="height:200px;overflow:auto;border:1px outset #ccc;">
                    <?php echo $outputHTMLContent ?>
                </div>
            <?php } ?>

            <?php require_once __DIR__ . '/admin.config.php' ?>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
</div>
