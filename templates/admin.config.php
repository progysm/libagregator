<?php
/**
 * @param HTMLOutputInterface $html
 * @param string $formActionSaveLink
 * @param array $config
 *   string name
 *   array $defines (key, value)
 *   array $tabFilInfo (should be sorted)
 */
if ($config) { ?>
    <form action="<?php echo $html->url($formActionSaveLink) ?>" method="post"><div>
    <input type="hidden" name="configFile" value="<?php echo $html->attr($config['name']) ?>" />
    <h3><?php echo $html->__html('Global Settings') ?></h3>
    <div id="agregatordefines">
    <?php foreach( $config['defines'] as $define => $value) { ?>
      <p><input type="hidden" name="DEFINETXT[]" value="<?php echo $html->attr($define) ?>"
      /><?php echo $html->html($define) ?>=<input type="text" name="DEFINE[]" value="<?php echo $html->attr($value) ?>" /></p>
    <?php } ?>
    </div>
  
    <h3><?php echo $html->__html('RSS feeds list') ?></h3>
    <table summary="<?php echo $html->__html('RSS feeds list') ?>" id="fil">
    <thead><tr>
      <th><?php echo $html->__html('RSS URL/Web Site') ?></th>
      <th><?php echo $html->__html('RSS Title') ?></th>
      <th><?php echo $html->__html('Entities') ?></th>
      <th><?php echo $html->__html('Description') ?></th>
      <th><?php echo $html->__html('Disabled') ?></th>
      <th><?php echo $html->__html('Error / times') ?></th>
      <th><?php echo $html->__html('Number of Items') ?></th>
      <th><?php echo $html->__html('Last count / date') ?></th>
    </tr></thead>
    <tbody>
    <tr><td><input type="text" value="" name="newrss0" class="url" /><br />
      <input type="text" value="" name="newsite0" class="url" /></td>
      <td><input type="text" value="" name="newname0" /></td>
    <td><input type="checkbox" name="newentities0" /></td>
    <td><input type="checkbox" name="newdescription0" /></td>
    <td><input type="checkbox" name="newdisabled0" /></td>
    <td></td><td></td><td></td>
    </tr>

    <?php
    $i = 0;
    foreach ($config['tabFilInfo'] as $fil) {
      $entitiesChecked = isset($fil['entities']) && $fil['entities'] ? ' checked="checked"' : '';
      $descriptionChecked = isset($fil['description']) && $fil['description'] ? ' checked="checked"' : '';
      $disabledChecked = isset($fil['disabled']) && $fil['disabled'] ? ' checked="checked"' : '';
      ?>
      <tr><td><input type="text" value="<?php echo $html->attr($fil[0]) ?>" name="rss<?php echo $i ?>" class="url" /><br />
      <input type="text" value="<?php echo $html->attr($fil[2]) ?>" name="site<?php echo $i ?>" class="url" /></td>
      <td><input type="text" value="<?php echo $html->attr($fil[1]) ?>" name="name<?php echo $i ?>" /></td>
      <td><input type="checkbox"<?php echo $entitiesChecked ?> name="entities<?php echo $i ?>" /></td>
      <td><input type="checkbox"<?php echo $descriptionChecked ?> name="description<?php echo $i ?>" /></td>
      <td><input type="checkbox"<?php echo $disabledChecked ?> name="disabled<?php echo $i ?>" /></td>
      <td><input type="text" value="<?php echo $html->attr($fil['error']) ?>" name="error<?php echo $i ?>" /><br />
      <input type="text" value="<?php echo $html->attr($fil['errorTimes']) ?>" name="errorTimes<?php echo $i ?>" class="integer" /></td>
      <td><input type="text" value="<?php echo $html->attr($fil['count']) ?>" name="count<?php echo $i ?>" class="integer" /></td>
      <td><input type="text" value="<?php echo $html->attr($fil['lastcount']) ?>" name="lastcount<?php echo $i ?>" class="integer" /><br />
      <input type="text" value="<?php echo $html->attr($fil['lastdate']) ?>" name="lastdate<?php echo $i ?>" class="date" /></td>
      </tr>

      <?php
      $i++;
    }
    ?>
    </tbody></table>
    <input type="hidden" name="newnbrss" value="1" />
    <input type="hidden" name="nbrss" value="<?php echo $i ?>" />
    <input type="submit" value="<?php echo $html->__html('Save') ?>" />
    </div></form>
<?php
} // if ConfigFile
?>
